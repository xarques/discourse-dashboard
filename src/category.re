open Configuration;

open Types;

module Styles = {
  open Css;
  let actions = style([display(flexBox), justifyContent(spaceBetween)]);
  let buttonToolbar = style([display(flexBox), justifyContent(center)]);
  let card =
    style([marginLeft(auto), marginRight(auto), maxWidth(px(345))]);
  let category = style([padding(px(8))]);
  let count = style([alignItems(center), display(flexBox)]);
  let media =
    style([
      display(block),
      height(px(200)),
      marginLeft(auto),
      marginRight(auto),
      important(width(auto)),
    ]);
};

let component = ReasonReact.statelessComponent("Category");

let make = (~configCategory: configCategory, _children) => {
  ...component,
  render: _self =>
    <div className=Styles.category>
      <MaterialUi.Toolbar className=Styles.buttonToolbar>
        <MaterialUi.Button
          color=`Primary
          component=(`String("a"))
          href=(
            Urls.resolve(
              config.apiUrl,
              "/new-topic?category_id="
              ++ string_of_int(configCategory.id)
              ++ "&body="
              ++ Js.Global.encodeURIComponent(configCategory.newTopicBody),
            )
          )
          size=`Large
          variant=`Raised>
          (ReasonReact.string(configCategory.newTopicButtonTitle))
        </MaterialUi.Button>
      </MaterialUi.Toolbar>
      <Fetcher
        url=(
          Urls.resolve(config.apiUrl, "/c/" ++ configCategory.slug ++ ".json")
        )>
        ...(
             response =>
               switch (response) {
               | Failed(error) =>
                 Js.log2("Fetch failed error: ", error);
                 <div> (ReasonReact.string("Fetch failed error")) </div>;
               | Loaded(data) =>
                 let category = Decoders.category(data);
                 <MaterialUi.Grid container=true spacing=MaterialUi.Grid.V16>
                   (
                     category.topicList.topics
                     |> Array.map((topicEmbed: topicEmbed) =>
                          <MaterialUi.Grid
                            item=true
                            key=(string_of_int(topicEmbed.id))
                            lg=MaterialUi.Grid.V3
                            md=MaterialUi.Grid.V4
                            sm=MaterialUi.Grid.V6
                            xl=MaterialUi.Grid.V2
                            xs=MaterialUi.Grid.V12>
                            <MaterialUi.Card className=Styles.card>
                              (
                                switch (topicEmbed.imageUrl) {
                                | None => ReasonReact.null
                                | Some(imageUrl) =>
                                  <MaterialUi.CardMedia
                                    className=Styles.media
                                    component=(`String("img"))
                                    image=imageUrl
                                  />
                                }
                              )
                              <MaterialUi.CardContent>
                                <MaterialUi.Typography
                                  component=(`String("h2"))
                                  gutterBottom=true
                                  variant=`Headline>
                                  (topicEmbed.title |> ReasonReact.string)
                                </MaterialUi.Typography>
                                <MaterialUi.Typography
                                  component=(`String("div"))>
                                  <div
                                    dangerouslySetInnerHTML={
                                      "__html": topicEmbed.excerpt,
                                    }
                                  />
                                </MaterialUi.Typography>
                              </MaterialUi.CardContent>
                              <MaterialUi.CardActions className=Styles.actions>
                                <span className=Styles.count>
                                  (
                                    ReasonReact.string(
                                      string_of_int(topicEmbed.likeCount)
                                      ++ {js| |js},
                                    )
                                  )
                                  <MaterialUIIcons.Favorite />
                                </span>
                                <span className=Styles.count>
                                  (
                                    ReasonReact.string(
                                      string_of_int(topicEmbed.postsCount - 1)
                                      ++ {js| |js},
                                    )
                                  )
                                  <MaterialUIIcons.Reply />
                                </span>
                                <MaterialUi.Button
                                  component=(`String("a"))
                                  href=(
                                    Urls.resolve(
                                      config.apiUrl,
                                      "/t/"
                                      ++ topicEmbed.slug
                                      ++ "/"
                                      ++ string_of_int(topicEmbed.id),
                                    )
                                  )
                                  size=`Small>
                                  (ReasonReact.string({js|En savoir plus|js}))
                                </MaterialUi.Button>
                              </MaterialUi.CardActions>
                            </MaterialUi.Card>
                          </MaterialUi.Grid>
                        )
                     |> ReasonReact.array
                   )
                 </MaterialUi.Grid>;
               | Loading => <div> <MaterialUi.LinearProgress /> </div>
               }
           )
      </Fetcher>
    </div>,
};
