open Configuration;

open Types;

module Styles = {
  open Css;
  let actions = style([display(flexBox), justifyContent(spaceBetween)]);
  let count = style([alignItems(center), display(flexBox)]);
  let paper =
    style([
      marginLeft(px(16)),
      marginRight(px(16)),
      paddingLeft(px(8)),
      paddingRight(px(8)),
    ]);
};

let component = ReasonReact.statelessComponent("Topic");

let make = (~configTopic: configTopic, _children) => {
  ...component,
  render: _self =>
    <div className="Topic">
      <Fetcher
        url=(
          Urls.resolve(
            config.apiUrl,
            "/t/"
            ++ configTopic.slug
            ++ "/"
            ++ string_of_int(configTopic.id)
            ++ ".json",
          )
        )>
        ...(
             response =>
               switch (response) {
               | Failed(error) =>
                 Js.log2("Fetch failed error: ", error);
                 <div> (ReasonReact.string("Fetch failed error")) </div>;
               | Loaded(data) =>
                 let topic = Decoders.topic(data);
                 <MaterialUi.Paper className=Styles.paper>
                   <MaterialUi.Typography
                     component=(`String("h1"))
                     gutterBottom=true
                     variant=`Display3>
                     (topic.title |> ReasonReact.string)
                   </MaterialUi.Typography>
                   <MaterialUi.Typography component=(`String("div"))>
                     <div
                       dangerouslySetInnerHTML={
                         "__html": topic.postStream.posts[0].cooked,
                       }
                     />
                   </MaterialUi.Typography>
                   <MaterialUi.Toolbar className=Styles.actions>
                     <span className=Styles.count>
                       (
                         ReasonReact.string(
                           string_of_int(topic.likeCount) ++ {js| |js},
                         )
                       )
                       <MaterialUIIcons.Favorite />
                     </span>
                     <span className=Styles.count>
                       (
                         ReasonReact.string(
                           string_of_int(topic.postsCount - 1) ++ {js| |js},
                         )
                       )
                       <MaterialUIIcons.Reply />
                     </span>
                     <MaterialUi.Button
                       component=(`String("a"))
                       href=(
                         Urls.resolve(
                           config.apiUrl,
                           "/t/"
                           ++ topic.slug
                           ++ "/"
                           ++ string_of_int(topic.id),
                         )
                       )
                       size=`Small>
                       (ReasonReact.string({js|En savoir plus|js}))
                     </MaterialUi.Button>
                   </MaterialUi.Toolbar>
                 </MaterialUi.Paper>;
               | Loading => <div> <MaterialUi.LinearProgress /> </div>
               }
           )
      </Fetcher>
    </div>,
};
