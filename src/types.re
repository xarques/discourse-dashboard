type configBanner = {
  buttonTitle: string,
  buttonUrl: string,
  imageUrl: string,
  title: string,
};

type configCategory = {
  id: int,
  index: int,
  newTopicBody: string,
  newTopicButtonTitle: string,
  slug: string,
  title: string,
};

type configTopic = {
  id: int,
  index: int,
  slug: string,
  title: string,
};

type configTab =
  | ConfigCategory(configCategory)
  | ConfigTopic(configTopic);

type config = {
  apiUrl: string,
  appTitle: string,
  appUrl: string,
  banner: option(configBanner),
  tabs: array(configTab),
  twitterName: string,
};

type thumbnails = {
  normal: string,
  retina: string,
};

type topicEmbed = {
  excerpt: string,
  id: int,
  imageUrl: option(string),
  likeCount: int,
  postsCount: int,
  slug: string,
  thumbnails: option(thumbnails),
  title: string,
};

type topicList = {topics: array(topicEmbed)};

type category = {topicList};

type post = {cooked: string};

type postStream = {posts: array(post)};

type topic = {
  fancyTitle: string,
  id: int,
  likeCount: int,
  postsCount: int,
  postStream,
  slug: string,
  title: string,
};
