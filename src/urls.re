/* [@bs.module "url-resolve"]
   external resolve : (string, string) => string = "resolve"; */
[@bs.module] external resolve : (string, string) => string = "url-resolve";
