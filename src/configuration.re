/* Load different configurations (for development, production, etc). */
%bs.raw
{|
const configFilename = process.env.NODE_ENV || "development"
|};

let config =
  Decoders.config(
    [%bs.raw {|require("../config/" + configFilename).default|}],
  );
