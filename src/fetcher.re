type action =
  | FailedAction(Js.Promise.error)
  | FetchAction
  | FetchedAction(Js.Json.t);

type state =
  | Failed(Js.Promise.error)
  | Loaded(Js.Json.t)
  | Loading;

let component = ReasonReact.reducerComponent("Fetcher");

let make = (~url: string, children) => {
  ...component,
  initialState: () => Loading,
  reducer: (action, _state) =>
    switch (action) {
    | FailedAction(error) => ReasonReact.Update(Failed(error))
    | FetchAction =>
      ReasonReact.UpdateWithSideEffects(
        Loading,
        (
          self =>
            Js.Promise.(
              Fetch.fetch(url)
              |> then_(Fetch.Response.json)
              |> then_(data =>
                   self.send(FetchedAction(data)) |> Js.Promise.resolve
                 )
              |> catch(error =>
                   Js.Promise.resolve(self.send(FailedAction(error)))
                 )
              |> ignore
            )
        ),
      )
    | FetchedAction(data) => ReasonReact.Update(Loaded(data))
    },
  didMount: ({send}) => send(FetchAction),
  render: ({state}) => children(state),
};
