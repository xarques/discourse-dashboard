open Types;

let configBanner = (json: Js.Json.t) : configBanner =>
  Json.Decode.{
    buttonTitle: json |> field("buttonTitle", string),
    buttonUrl: json |> field("buttonUrl", string),
    imageUrl: json |> field("imageUrl", string),
    title: json |> field("title", string),
  };

let configCategory = (json: Js.Json.t) : configCategory =>
  Json.Decode.{
    id: json |> field("id", int),
    index: 0,
    newTopicBody: json |> field("newTopicBody", string),
    newTopicButtonTitle: json |> field("newTopicButtonTitle", string),
    slug: json |> field("slug", string),
    title: json |> field("title", string),
  };

let configTopic = (json: Js.Json.t) : configTopic =>
  Json.Decode.{
    id: json |> field("id", int),
    index: 0,
    slug: json |> field("slug", string),
    title: json |> field("title", string),
  };

let configTab = (json: Js.Json.t) : configTab =>
  switch (Json.Decode.(field("type", string, json))) {
  | "category" => ConfigCategory(configCategory(json))
  | "topic" => ConfigTopic(configTopic(json))
  | somethingElse =>
    raise @@ Json.Decode.DecodeError("Unknown tab type: " ++ somethingElse)
  };

let config = (json: Js.Json.t) : config =>
  Json.Decode.{
    apiUrl: json |> field("apiUrl", string),
    appTitle: json |> field("appTitle", string),
    appUrl: json |> field("appUrl", string),
    banner: json |> optional(field("banner", configBanner)),
    tabs:
      json
      |> field("tabs", array(configTab))
      |> Array.mapi((index, tab: configTab) =>
           switch (tab) {
           | ConfigCategory(configCategory) =>
             ConfigCategory({...configCategory, index})
           | ConfigTopic(configTopic) => ConfigTopic({...configTopic, index})
           }
         ),
    twitterName: json |> field("twitterName", string),
  };

let thumbnails = (json: Js.Json.t) : thumbnails =>
  Json.Decode.{
    normal: json |> field("normal", string),
    retina: json |> field("retina", string),
  };

let topicEmbed = (json: Js.Json.t) : topicEmbed =>
  Json.Decode.{
    excerpt: json |> field("excerpt", string),
    id: json |> field("id", int),
    imageUrl: json |> optional(field("image_url", string)),
    likeCount: json |> field("like_count", int),
    postsCount: json |> field("posts_count", int),
    slug: json |> field("slug", string),
    thumbnails: json |> optional(field("thumbnails", thumbnails)),
    title: json |> field("title", string),
  };

let topicList = (json: Js.Json.t) : topicList =>
  Json.Decode.{topics: json |> field("topics", array(topicEmbed))};

let category = (json: Js.Json.t) : category =>
  Json.Decode.{
    /* primary_groups, */
    topicList: json |> field("topic_list", topicList),
    /* users, */
  };

let post = (json: Js.Json.t) : post =>
  Json.Decode.{cooked: json |> field("cooked", string)};

let postStream = (json: Js.Json.t) : postStream =>
  Json.Decode.{posts: json |> field("posts", array(post))};

let topic = (json: Js.Json.t) : topic =>
  Json.Decode.{
    fancyTitle: json |> field("fancy_title", string),
    id: json |> field("id", int),
    likeCount: json |> field("like_count", int),
    postsCount: json |> field("posts_count", int),
    postStream: json |> field("post_stream", postStream),
    slug: json |> field("slug", string),
    title: json |> field("title", string),
  };
