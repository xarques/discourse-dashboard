open Configuration;

open Types;

type page =
  | NotFoundPage(list(string), string)
  | TabPage(configTab);

type action =
  | Navigate(page);

type state = page;

module Styles = {
  open Css;
  let app =
    style([
      display(flexBox),
      flexDirection(column),
      minHeight(vh(100.0)),
    ]);
  let banner =
    switch (config.banner) {
    | None => style([])
    | Some(banner) =>
      style([
        alignItems(center),
        backgroundImage(url(banner.imageUrl)),
        backgroundSize(cover),
        display(flexBox),
        height(vh(100.0)),
        justifyContent(center),
      ])
    };
  let bannerContent =
    style([color(white), maxWidth(pct(50.0)), textAlign(center)]);
  let buttonToolbar =
    style([display(flexBox), justifyContent(center), margin(px(32))]);
  let footer =
    style([
      color(white),
      backgroundColor(rgb(134, 142, 150)),
      marginTop(px(16)),
      paddingTop(px(16)),
    ]);
  let footerContent = style([textAlign(center)]);
  let tabs = style([flexGrow(1)]);
};

let tabToUrlPath = (tab: configTab) : string =>
  "/"
  ++ (
    switch (tab) {
    | ConfigCategory(configCategory) =>
      configCategory.index === 0 ? "" : configCategory.slug
    | ConfigTopic(configTopic) =>
      configTopic.index === 0 ? "" : configTopic.slug
    }
  );

let urlToPage = (url: ReasonReact.Router.url) : page =>
  switch (url.path) {
  | [] => TabPage(config.tabs[0])
  | [slug] =>
    config.tabs
    |> Array.to_list
    |> (
      tabs =>
        switch (
          List.find(
            tab =>
              switch (tab) {
              | ConfigCategory(configCategory) => configCategory.slug === slug
              | ConfigTopic(configTopic) => configTopic.slug === slug
              },
            tabs,
          )
        ) {
        | tab => TabPage(tab)
        | exception Not_found => NotFoundPage(url.path, url.hash)
        }
    )
  | _ => NotFoundPage(url.path, url.hash)
  };

let component = ReasonReact.reducerComponent("App");

let make = _children => {
  ...component,
  initialState: () =>
    urlToPage(ReasonReact.Router.dangerouslyGetInitialUrl()),
  subscriptions: self => [
    Sub(
      () =>
        ReasonReact.Router.watchUrl(url =>
          self.send(Navigate(urlToPage(url)))
        ),
      ReasonReact.Router.unwatchUrl,
    ),
  ],
  reducer: (action, _state) =>
    switch (action) {
    | Navigate(page) => ReasonReact.Update(page)
    },
  render: self =>
    <div className=Styles.app>
      <MaterialUi.CssBaseline />
      <MaterialUi.AppBar position=`Static>
        <MaterialUi.Toolbar>
          <MaterialUi.Typography color=`Inherit variant=`Title>
            (config.appTitle |> ReasonReact.string)
          </MaterialUi.Typography>
        </MaterialUi.Toolbar>
      </MaterialUi.AppBar>
      (
        switch (config.banner) {
        | None => ReasonReact.null
        | Some(banner) =>
          <div className=Styles.banner>
            <div className=Styles.bannerContent>
              <MaterialUi.Typography
                color=`Inherit component=(`String("div")) variant=`Display3>
                (banner.title |> ReasonReact.string)
              </MaterialUi.Typography>
              <MaterialUi.Toolbar className=Styles.buttonToolbar>
                <MaterialUi.Button
                  color=`Primary
                  component=(`String("a"))
                  href=banner.buttonUrl
                  size=`Large
                  variant=`Raised>
                  (ReasonReact.string(banner.buttonTitle))
                </MaterialUi.Button>
              </MaterialUi.Toolbar>
            </div>
          </div>
        }
      )
      (
        switch (self.state) {
        | NotFoundPage(_path, _hash) =>
          <p> ("Page not found!" |> ReasonReact.string) </p>
        | TabPage(tab) =>
          <div className=Styles.tabs>
            <MaterialUi.Tabs
              centered=true
              onChange=(
                (_event, value) =>
                  ReasonReact.Router.push(tabToUrlPath(config.tabs[value]))
              )
              theme={"todo": "todo"}
              value=(
                switch (tab) {
                | ConfigCategory(configCategory) => configCategory.index
                | ConfigTopic(configTopic) => configTopic.index
                }
              )>
              (
                config.tabs
                |> Array.map(tab =>
                     switch (tab) {
                     | ConfigCategory(configCategory) =>
                       <MaterialUi.Tab
                         key=(string_of_int(configCategory.index))
                         label=(ReasonReact.string(configCategory.title))
                         value=configCategory.index
                       />
                     | ConfigTopic(configTopic) =>
                       <MaterialUi.Tab
                         key=(string_of_int(configTopic.index))
                         label=(ReasonReact.string(configTopic.title))
                         value=configTopic.index
                       />
                     }
                   )
                |> ReasonReact.array
              )
            </MaterialUi.Tabs>
            (
              switch (tab) {
              | ConfigCategory(configCategory) =>
                <Category key=configCategory.slug configCategory />
              | ConfigTopic(configTopic) =>
                <Topic key=(string_of_int(configTopic.id)) configTopic />
              }
            )
          </div>
        }
      )
      <footer className=Styles.footer>
        <div className=Styles.footerContent>
          <div>
            (
              ReasonReact.string(
                {js|Discourse-Dashboard — A front page based on Discourse API|js},
              )
            )
          </div>
          <div>
            (
              ReasonReact.string(
                {js|Discourse-Dashboard is free and open source software. |js},
              )
            )
            <a
              href="https://framagit.org/parlement-ouvert/discourse-dashboard">
              (ReasonReact.string("Contribute!"))
            </a>
          </div>
        </div>
      </footer>
    </div>,
};
