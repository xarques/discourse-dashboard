import config from "./global"

Object.assign(config, {
  appTitle: "#dataFin (dev)",
})

export default config
