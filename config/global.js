export default {
  apiUrl: "https://datafin.parlement-ouvert.fr/",
  appTitle: "#dataFin",
  appUrl: "http://localhost:3000/",
  banner: {
    buttonTitle: "Inscrivez-vous au hackathon",
    buttonUrl: "https://datafin.parlement-ouvert.fr/",
    imageUrl: "https://forum.parlement-ouvert.fr/uploads/default/original/1X/ef961b9138c552ea3224a447f5c04a7842ec8951.jpg",
    title: "Exploitons les données financières publiques !",
  },
  tabs: [
    {
      type: "category",
      id: 6,
      newTopicBody: "![Logo du défi](upload://xUatt6HolcDqVnPpRQXl1vbG6if.jpg)\n\nDESCRIPTION_DEFI\n\n* [Site](LIEN_VERS_SITE)\n* [Dépôt du code source](LIEN_VERS_DEPOT)\n* Licence du logiciel : [NOM_LICENCE](LIEN_VERS_LICENCE)\n\n## Jeux de données utilisés\n\nhttps://www.data.gouv.fr/fr/datasets/le-budget-analyse-et-evolution-00000000/\n\nhttps://www.data.gouv.fr/fr/datasets/ID_JEU_DE_DONNEES/\n\n## Besoins\n\n* data scientists\n* designers\n* développeurs\n* économistes\n* ergonomes\n* graphistes\n* journalistes\n",
      newTopicButtonTitle: "Proposer un nouveau défi",
      slug: "defis",
      title: "Défis",
    },
    {
      type: "category",
      id: 5,
      newTopicBody: "![Logo du jeu de données](upload://xUatt6HolcDqVnPpRQXl1vbG6if.jpg)\n\nDESCRIPTION\n\n* [Jeu de données](LIEN_VERS_SITE)\n* Licence des données : [NOM_LICENCE](LIEN_VERS_LICENCE)\n",
      newTopicButtonTitle: "Ajouter un jeu de données",
      slug: "jeux-de-donnees",
      title: "Jeux de données",
    },
    {
      type: "topic",
      id: 8,
      slug: "bienvenue-au-hackathon-datafin",
      title: "Informations pratiques",
    },
    // {
    //   type: "topic",
    //   id: 24,
    //   slug: "a-propos",
    //   title: "À propos",
    // },
  ],
  twitterName: "#dataFin",
}
